export class locations {
    constructor() {
        this.list = [];
        this.selectors();
        this.events();
    }

    selectors() {
        this.form = document.querySelector(".form__form");
        this.items = document.querySelector(".locations-items");
        this.imageInput = document.querySelector("#locationImage");
        this.titleInput = document.querySelector("#locationTitle");
        this.descriptionInput = document.querySelector("#locationDescription");
    }

    events() {
        this.form.addEventListener("submit", addItemToList);
    }

    addItemToList(event) {
        event.preventDefault();

        const locationImage = this.imageInput.files[0];
        const locationTitle = event.target["locationTitle"].value;
        const locationDescription = event.target["locationDescription"].value;

        if (locationName !== "") {
            const item = {
                Image: locationImage,
                name: locationTitle,
                description: locationDescription,
            };

            this.list.push(item);

            this.renderListItems();
            this.resetInputs();
        }
    }

    renderListItems() {
        let itemsStructure = "";

        this.list.forEach(function (item) {
            var uri = URL.createObjectURL(item.Image);
            itemsStructure += `
                    <li class="locations-item">
                            <img src="${uri}"></img>
                            <div class="content">
                                <h2 class="title">${item.name}</h2>
                                <p class="description">${item.description}</p>
                            </div>
                    </li>

                `;
        });

        this.items.innerHTML = itemsStructure;
    }

    resetInputs() {
        this.imageInput = "";
        this.titleInput = "";
        this.descriptionInput = "";
    }
}
