document.addEventListener("DOMContentLoaded", function () {
    const list = [];
    const form = document.querySelector(".form__form");
    const items = document.querySelector(".locations-items");
    //inputs
    const imageInput = document.querySelector("#locationImage");
    const titleInput = document.querySelector("#locationImage");
    const descriptionInput = document.querySelector("#locationImage");

    form.addEventListener("submit", addItemToList);

    function addItemToList(event) {
        event.preventDefault();

        const locationImage = imageInput.files[0];
        const locationName = event.target["locationName"].value;
        const locationDescription = event.target["locationDescription"].value;

        if (locationName !== "") {
            const item = {
                Image: locationImage,
                name: locationName,
                description: locationDescription,
            };

            list.push(item);

            renderListItems();
            resetInputs();
        }
    }

    function renderListItems() {
        let itemsStructure = "";

        list.forEach(function (item) {
            var uri = URL.createObjectURL(item.Image);
            itemsStructure += `
                    <li class="locations-item">
                            <img src="${uri}"></img>
                            <div class="content">
                                <h2 class="title">${item.name}</h2>
                                <p class="description">${item.description}</p>
                            </div>
                    </li>

                `;
        });

        items.innerHTML = itemsStructure;
    }

    function resetInputs() {
        imageInput = "";
        titleInput = "";
        descriptionInput = "";
    }
});
